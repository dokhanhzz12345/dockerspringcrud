# DockerSpringCrud

Spring Boot JPA MySQL Docker

Chuẩn bị image

docker pull mysql:5.7

Deploy Spring Boot + MYSQL Application to Docker

=====================================================================================

bước 1:-docker run --name docker-mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=test -e MYSQL_USER=user -e MYSQL_PASSWORD=password -d mysql:5.7 --default-authentication-plugin=mysql_native_password
	(khởi chạy mysql  container)
	
bước 2:-docker build . -t dokhanhzz/spring-boot-crud (build image "dokhanhzz/spring-boot-crud" là tên image)

bước 3:-docker run -p 9000:8090 --name final-app --link docker-mysql:mysql dokhanhzz/spring-boot-crud

Nếu tải image về thì chỉ cần chạy bước 1 và bước 3

Note: trong file application.properties hoặc application.yml: spring.datasource.url = jdbc:mysql://docker-mysql:3306/test?useSSL=false (chỗ docker-mysql giống với name đã đặt ở bước 1)
